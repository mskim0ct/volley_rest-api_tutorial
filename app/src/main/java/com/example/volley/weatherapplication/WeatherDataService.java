package com.example.volley.weatherapplication;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WeatherDataService {

    public static final String QUERY_FOR_CITY_ID = "https://www.metaweather.com/api/location/search/?query=";
    public static final String QUERY_FOR_CITY_WEATHER_BY_ID = "https://www.metaweather.com/api/location/";

    Context context;
    public WeatherDataService(Context context) {
        this.context = context;
    }

    public interface VolleyListener{
        void onError(String message);
        void onResponse(String cityID);
    }

    public void getCityID(String cityName, VolleyListener volleyListener){
        String url = QUERY_FOR_CITY_ID +cityName;
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                String cityID = "";
                try {
                    JSONObject cityInfo = response.getJSONObject(0);
                    cityID = cityInfo.getString("woeid");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                volleyListener.onResponse(cityID);
                //Toast.makeText(context, "City ID = "+cityID, Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyListener.onError("error");
                //Toast.makeText(context, "error", Toast.LENGTH_SHORT).show();
            }
        });
        MySingleton.getInstance(context).addToRequestQueue(request);
    }


    public interface VolleyForecastByIdListener{
        void onError(String message);
        void onResponse(List<WeatherReportModel> report);
    }

    public void getCityForecastByID(String cityID, VolleyForecastByIdListener listener){
        List<WeatherReportModel> report = new ArrayList<>();

        String url = QUERY_FOR_CITY_WEATHER_BY_ID+cityID;
        Gson gson = new Gson();
        // get the json object
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray consolidated_weather_list = response.getJSONArray("consolidated_weather");
                    for(int i = 0 ; i < consolidated_weather_list.length() ; i++){
//                      WeatherReportModel weatherReportModel = new WeatherReportModel();
                        JSONObject jsonObj = (JSONObject)consolidated_weather_list.get(i);
                        WeatherReportModel weatherReportModel = gson.fromJson(jsonObj.toString(), WeatherReportModel.class);

//                        weatherReportModel.setAir_pressure((float)jsonObj.getDouble("air_pressure"));
//                        weatherReportModel.setApplicable_date(jsonObj.getString("applicable_date"));
//                        weatherReportModel.setCreated(jsonObj.getString("created"));
//                        weatherReportModel.setHumidity(jsonObj.getInt("humidity"));
//                        weatherReportModel.setId(jsonObj.getInt("id"));
//                        weatherReportModel.setMax_temp((float)jsonObj.getDouble("max_temp"));
//                        weatherReportModel.setMin_temp((float)jsonObj.getDouble("min_temp"));
//                        weatherReportModel.setPredictability(jsonObj.getInt("predictability"));
//                        weatherReportModel.setThe_temp((float)jsonObj.getDouble("the_temp"));
//                        weatherReportModel.setVisibility((float)jsonObj.getDouble("visibility"));
//                        weatherReportModel.setWeather_state_abbr(jsonObj.getString("weather_state_abbr"));
//                        weatherReportModel.setWeather_state_name(jsonObj.getString("weather_state_name"));
//                        weatherReportModel.setWind_direction((float)jsonObj.getDouble("wind_direction"));
//                        weatherReportModel.setWind_direction_compass(jsonObj.getString("wind_direction_compass"));
//                        weatherReportModel.setWind_speed((float)jsonObj.getDouble("wind_speed"));
                        report.add(weatherReportModel);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                listener.onResponse(report);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError("error");
            }
        });
        MySingleton.getInstance(context).addToRequestQueue(request);
        //get the property called "consolodated_weather" which is an Array
        //get each item in the array and assign it to a new WeatherReportModel Object
        //return report;
    }


    public interface GetCityForecastByNameCallback{
        void onError(String message);
        void onResponse(List<WeatherReportModel> report);
    }

    /* callback 지옥에 빠진다..... callback 안에 또 callback .... callback 지옥에서 벗어나기 위해서는
    * 다른 언어에서 제공하는 await, async를 적용하도록 한다.*/
    public void getCityForecastByName(String cityName, final GetCityForecastByNameCallback callback){
        //fetch the city id given the city name.
        getCityID(cityName, new VolleyListener() {
            @Override
            public void onError(String message) {

            }

            @Override
            public void onResponse(String cityID) {
                //now we have the city id!
                getCityForecastByID(cityID, new VolleyForecastByIdListener() {
                    @Override
                    public void onError(String message) {

                    }

                    @Override
                    public void onResponse(List<WeatherReportModel> report) {
                        //we have the weather report.
                        callback.onResponse(report);
                    }
                });
            }
        });

        //fetch the city forecast given the city id.
    }
}
