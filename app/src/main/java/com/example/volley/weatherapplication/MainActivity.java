package com.example.volley.weatherapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button cityIdBtn, weatherIdBtn, weatherNameBtn;
    EditText cityNameEditText;
    ListView weatherListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        cityIdBtn = findViewById(R.id.id_cityId);
        weatherIdBtn = findViewById(R.id.id_weatherId);
        weatherNameBtn = findViewById(R.id.id_weatherName);
        cityNameEditText = findViewById(R.id.et_cityName);
        weatherListView = findViewById(R.id.lv_weatherReports);

        cityIdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WeatherDataService weatherDataService = new WeatherDataService(MainActivity.this);
                weatherDataService.getCityID(cityNameEditText.getText().toString(), new WeatherDataService.VolleyListener(){

                    @Override
                    public void onError(String message) {
                        Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String cityID) {
                        Toast.makeText(getApplicationContext(), "returned id for "+cityID, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        weatherIdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WeatherDataService weatherDataService = new WeatherDataService(MainActivity.this);
                weatherDataService.getCityForecastByID(cityNameEditText.getText().toString(), new WeatherDataService.VolleyForecastByIdListener() {
                    @Override
                    public void onError(String message) {
                        Toast.makeText(MainActivity.this, "error", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(List<WeatherReportModel> report) {
                        // put the entire list into the listview control
                        ArrayAdapter arrayAdapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, report);
                        weatherListView.setAdapter(arrayAdapter);
                    }
                });
            }
        });
        weatherNameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WeatherDataService weatherDataService = new WeatherDataService(MainActivity.this);
                weatherDataService.getCityForecastByName(cityNameEditText.getText().toString(), new WeatherDataService.GetCityForecastByNameCallback() {
                    @Override
                    public void onError(String message) {

                    }

                    @Override
                    public void onResponse(List<WeatherReportModel> report) {
                        ArrayAdapter adapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, report);
                        weatherListView.setAdapter(adapter);
                    }
                });
            }
        });
    }
}